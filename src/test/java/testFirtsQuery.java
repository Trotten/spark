import Utils.ReadFromArgs;
import org.apache.spark.sql.sources.In;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.*;

public class testFirtsQuery {

    @Test
    public void testFirst() throws IOException {
        /*String string="prova/ciao";
        System.out.println(string.substring(0,string.indexOf("/")));
        System.out.println(string.substring(string.indexOf("/")+1));*/


        String fileNamePrimaPresa6Ore = "resultPrimaPresa6Ore.txt";
        String fileNameSecondaPresa6Ore = "resultSecondaPresa6Ore.csv";
        String fileNameTerzaPresa6Ore = "resultTerzaPresa6Ore.csv";

        String csvFile = "C:\\Users\\Emiliano\\Desktop\\d14_filtered.csv";
        String line = "";
        String cvsSplitBy = ",";
        long initialTimeStamp = 1377986420;
        int iteration = 0, sixHours = 21600;


        Double consumoPrimaPresa6Ore = 0.0, consumoSecondaPresa6Ore = 0.0, consumoTerzaPresa6Ore = 0.0;
        Double consumoPrimaPresaTotale = 0.0, consumoSecondaPresaTotale = 0.0, consumoTerzaPresaTotale = 0.0;
        long totaleDatiPrimaPresa6Ore = 0, totaleDatiSecondaPresa6Ore = 0, totaleDatiTerzaPresa6Ore = 0;
        long totaleDatiPrimaPresaTotale = 0, totaleDatiSecondaPresaTotale = 0, totaleDatiTerzaPresaTotale = 0;

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            FileWriter fwPrimaPresa6Ore, fwSecondaPresa6Ore, fwTerzaPresa6Ore;
            try {
                fwPrimaPresa6Ore = new FileWriter(fileNamePrimaPresa6Ore);
                fwSecondaPresa6Ore = new FileWriter(fileNameSecondaPresa6Ore);
                fwTerzaPresa6Ore = new FileWriter(fileNameTerzaPresa6Ore);

                while ((line = br.readLine()) != null) {

                    // use comma as separator
                    // 1377986520 < initialTimeStamp+(21)
                    String[] smart_plug = line.split(cvsSplitBy);
                    if(Long.valueOf(smart_plug[1])>=initialTimeStamp+(sixHours*iteration)){
                        fwPrimaPresa6Ore.write("\n\n");
                        fwPrimaPresa6Ore.write("Consumo 1° presa - 6 ore - casa 9: "
                                +consumoPrimaPresa6Ore+"\tMedia: "+consumoPrimaPresa6Ore/totaleDatiPrimaPresa6Ore);
                        fwPrimaPresa6Ore.write("\nIterazioni: "
                                +totaleDatiPrimaPresa6Ore+"\n\n");

                        consumoPrimaPresaTotale+=consumoPrimaPresa6Ore;
                        totaleDatiPrimaPresaTotale+=totaleDatiPrimaPresa6Ore;
                        consumoPrimaPresa6Ore=0.0;
                        totaleDatiPrimaPresa6Ore=0;
                        iteration++;


                        fwSecondaPresa6Ore.write("\n\n");
                        fwSecondaPresa6Ore.write("Consumo 2° presa - 6 ore - casa 9: "
                                +consumoSecondaPresa6Ore+"\tMedia: "+consumoSecondaPresa6Ore/totaleDatiSecondaPresa6Ore);
                        fwSecondaPresa6Ore.write("\nIterazioni: "
                                +totaleDatiSecondaPresa6Ore+"\n\n");

                        consumoSecondaPresaTotale+=consumoSecondaPresa6Ore;
                        totaleDatiSecondaPresaTotale+=totaleDatiSecondaPresa6Ore;
                        consumoSecondaPresa6Ore=0.0;
                        totaleDatiSecondaPresa6Ore=0;
                        iteration++;


                        fwTerzaPresa6Ore.write("\n\n");
                        fwTerzaPresa6Ore.write("Consumo 3° presa - 6 ore - casa 9: "
                                +consumoTerzaPresa6Ore+"\tMedia: "+consumoTerzaPresa6Ore/totaleDatiTerzaPresa6Ore);
                        fwTerzaPresa6Ore.write("\nIterazioni: "
                                +totaleDatiTerzaPresa6Ore+"\n\n");

                        consumoTerzaPresaTotale+=consumoTerzaPresa6Ore;
                        totaleDatiTerzaPresaTotale+=totaleDatiTerzaPresa6Ore;
                        consumoTerzaPresa6Ore=0.0;
                        totaleDatiTerzaPresa6Ore=0;
                        iteration++;
                    }

                    if(smart_plug[6].equals("9") && smart_plug[4].equals("0") && smart_plug[3].equals("1")){
                        fwPrimaPresa6Ore.write(line);
                        fwPrimaPresa6Ore.write("\n");
                        consumoPrimaPresa6Ore+=Double.valueOf(smart_plug[2]);
                        totaleDatiPrimaPresa6Ore++;
                    }else if(smart_plug[6].equals("9") && smart_plug[4].equals("1") && smart_plug[3].equals("0")) {
                        fwSecondaPresa6Ore.write(line);
                        fwSecondaPresa6Ore.write("\n");
                        consumoSecondaPresa6Ore+=Double.valueOf(smart_plug[2]);
                        totaleDatiSecondaPresa6Ore++;
                    }else if(smart_plug[6].equals("9") && smart_plug[4].equals("2") && smart_plug[3].equals("0")) {
                        fwTerzaPresa6Ore.write(line);
                        fwTerzaPresa6Ore.write("\n");
                        consumoTerzaPresa6Ore+=Double.valueOf(smart_plug[2]);
                        totaleDatiTerzaPresa6Ore++;
                    }
                }

                fwPrimaPresa6Ore.write("\n");
                fwPrimaPresa6Ore.write("Consumo totale 1° presa casa 9: "
                        +consumoPrimaPresaTotale+"\tMedia: "+consumoPrimaPresaTotale/totaleDatiPrimaPresaTotale);
                fwPrimaPresa6Ore.write("\nIterazioni: "
                        +totaleDatiPrimaPresaTotale);

                fwSecondaPresa6Ore.write("\n\n");
                fwSecondaPresa6Ore.write("Consumo totale 2° presa casa 9: "
                        +consumoSecondaPresaTotale+"\tMedia: "+consumoSecondaPresaTotale/totaleDatiSecondaPresaTotale);
                fwSecondaPresa6Ore.write("\nIterazioni: "
                        +totaleDatiSecondaPresaTotale+"\n\n");


                fwTerzaPresa6Ore.write("\n\n");
                fwTerzaPresa6Ore.write("Consumo totale 3° presa casa 9: "
                        +consumoTerzaPresaTotale+"\tMedia: "+consumoTerzaPresaTotale/totaleDatiTerzaPresaTotale);
                fwTerzaPresa6Ore.write("\nIterazioni: "
                        +totaleDatiTerzaPresaTotale+"\n\n");

                fwPrimaPresa6Ore.close();
                fwSecondaPresa6Ore.close();
                fwTerzaPresa6Ore.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }



    }

}
