import Utils.ReadFromArgs;
import org.apache.hadoop.mapred.FileAlreadyExistsException;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;
import scala.Tuple3;
import scala.Tuple4;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class Main {


    private static JavaRDD<SmartPlug> smartPlugJavaRDDInstant;
    private static JavaRDD<SmartPlug> smartPlugJavaRDDCumulative;


    public static void main(String[] args) {

        //leggo gli argomenti passati come input e li mappo
        Map<String, String> pathFiles = ReadFromArgs.readArgs(args);
        String pathInput = pathFiles.get("pathDataset");
        String pathOutputQuery1 = pathFiles.get("pathOutQuery1");
        String pathOutputQuery2 = pathFiles.get("pathOutQuery2");
        String pathOutputQuery3 = pathFiles.get("pathOutQuery3");
        try {
            ReadFromArgs.checkPathStrings(pathInput, pathOutputQuery1, pathOutputQuery2, pathOutputQuery3);
        } catch (IOException e) {

            //nel caso in cui non venissero inseriti tutti gli argomenti essenziali per l'esecuzione del programma, viene sollevata una eccezione
            e.printStackTrace();
            System.err.println("usage: --pathDataset fileInput --pathOutQuery1 fileOutputquery1" +
                    " --pathOutQuery2 fileOutputquery2 --pathOutQuery3 fileOutputquery3");
            System.exit(1);
        }


        SparkConf sparkConf = new SparkConf().setAppName("WordCount").setMaster("local[3]");
        JavaSparkContext javaSparkContext = new JavaSparkContext(sparkConf);


        //all'interno di questo try vengono avviati gli operatori di spark per la risoluzione delle tre query.
        //Nel caso in cui venga selezionata una directory già esistente, spark solleverà una eccezione.
        //È' stato scelto di non implementare le api di google cloud storage all'interno di spark per controllare
        //la presenza di una directory già esistente per non appesantire troppo il lavoro di spark
        try {
            Long time = System.currentTimeMillis();

            //parseFile(javaSparkContext,"gs://dataproc-e233f4d8-993e-4bc1-b672-76687e9fd607-europe-west2/d14_filtered.csv");
            parseFile(javaSparkContext, pathInput);

            stampaTempo(time, "Tempo trascorso a parsare il file");
            time = System.currentTimeMillis();

            firstQuery(pathOutputQuery1);
            stampaTempo(time, "Tempo trascorso a effettuare la prima query");
            time = System.currentTimeMillis();

            secondQuery(pathOutputQuery2);
            stampaTempo(time, "Tempo trascorso a effettuare la seconda query");
            time = System.currentTimeMillis();

            thirdQuery(pathOutputQuery3);
            stampaTempo(time, "Tempo trascorso a effettuare la terza query");
        } catch (FileAlreadyExistsException e) {
            System.err.println("directory output già esistente\n\n\n\n");

            e.printStackTrace();

        }

        javaSparkContext.stop();

    }

    /**
     * Metodo creato per stampare il tempo. Viene calcolata la differenza tra il tempo istantaneo e quello passato come input
     *
     * @param time        tempo di input da cui calcolare la differenza
     * @param description
     */

    public static void stampaTempo(Long time, String description) {
        Long myTime = System.currentTimeMillis();
        System.out.println(description + " " + ((double) (myTime - time) / (double) 1000));
    }

    /**
     * Questo metodo serve per parsare il file csv (il cui path viene passato come in input).
     * Qui vengono fatti tutti i controlli di filtraggio. Alla fine del metodo vengono messi in cache
     * due JavaPairRDD, uno contentenente tutte le tuple con proprerty=0 e uno contenente tutte le tuple con property=1.
     * È' stato deciso di usare un oggetto custom SmartPlug, il quale contiene tutti i parametri delle colonne del file csv.
     *
     * @param javaSparkContext Context di java spark (già inizializzato in precedenza)
     * @param path             path del file da parsare
     */

    public static void parseFile(@NotNull JavaSparkContext javaSparkContext, String path) {

        //vengono lette le singole linee del file di input. A partire da questo punto ci saranno una serie di entità (che sarebbero le row del csv) da parsare e da inserire in uno SmartPlug.
        JavaRDD<String> lines = javaSparkContext.textFile(path);

        //filtro tutte le righe che non hanno 7 colonne. In tal caso suppongo che la riga sia mal formattata, perciò la scarto
        JavaRDD<String> checkcorrect = lines.filter(new Function<String, Boolean>() {
            @Override
            public Boolean call(String s) throws Exception {
                String[] strings = s.split(",");
                if (strings.length != 7)
                    return false;
                //da controllare che tutti i valori siano corretti
                return true;
            }
        })      //filtro tutte le righe che hanno un value > di un certo valore impostato nelle variabili globali (supposto che fosse 10000 KWh) o che sono con value <0.
                .filter(new Function<String, Boolean>() {
                    @Override
                    public Boolean call(String s) throws Exception {
                        String[] strings = s.split(",");
                        try {
                            int i = 0;
                            for (String string : strings) {
                                if (i == 2) {
                                    if (Double.valueOf(string) > GLOBAL.filterValueMax) {
                                        return false;
                                    }
                                }
                                if (Double.valueOf(string) < 0) {
                                    return false;
                                }
                                i++;
                            }
                        } catch (ClassCastException e) {
                            System.err.println(e.getCause() + " errore cast");
                            return false;
                        }
                        return true;
                    }
                });

        //javaRDD per riga con property=1 (instantaneo)
        JavaRDD<String> parsedPropertyInstant = checkcorrect
                //in questo JavaRDD inserisco tutte le tuple avente property=1
                .filter(new Function<String, Boolean>() {
                    @Override
                    public Boolean call(String s) throws Exception {
                        String[] strings = s.split(",");
                        try {
                            Boolean valueOfRowProperty = Boolean.valueOf(strings[3]);
                            if (Integer.valueOf(strings[3]) == 1)
                                return true;
                        } catch (ClassCastException e) {
                            System.err.println(e.getCause() + " Property mal formattato");
                            return false;
                        }
                        return false;

                    }
                });

        //javaRDD per riga con property=0 (cumulativo)
        JavaRDD<String> parsedPropertyCumulative = checkcorrect
                //in questo JavaRDD inserisco tutte le tuple avente property=0
                .filter(new Function<String, Boolean>() {
                    @Override
                    public Boolean call(String s) throws Exception {
                        String[] strings = s.split(",");
                        try {
                            Boolean valueOfRowProperty = Boolean.valueOf(strings[3]);
                            if (Integer.valueOf(strings[3]) == 0)
                                return true;
                        } catch (ClassCastException e) {
                            System.err.println(e.getCause() + " Property mal formattato");
                            return false;
                        }
                        return false;

                    }
                });

        //prendo la tupla filtrata e creo una nuova tupla in output contenente l'oggetto SmartPlug, i cui attributi sono le colonne della riga csv.
        smartPlugJavaRDDInstant = parsedPropertyInstant
                .flatMap(new FlatMapFunction<String, SmartPlug>() {
                    @Override
                    public Iterator<SmartPlug> call(String s) throws Exception {
                        String[] strings = s.split(",");
                        SmartPlug smartPlug = new SmartPlug(Long.valueOf(strings[0]), Long.valueOf(strings[1]), Float.valueOf(strings[2]), Boolean.valueOf(strings[3]),
                                Long.valueOf(strings[4]), Long.valueOf(strings[5]), Long.valueOf(strings[6]));

                        return Arrays.asList(smartPlug).iterator();
                    }
                });

        smartPlugJavaRDDCumulative = parsedPropertyCumulative
                .flatMap(new FlatMapFunction<String, SmartPlug>() {
                    @Override
                    public Iterator<SmartPlug> call(String s) throws Exception {
                        String[] strings = s.split(",");
                        SmartPlug smartPlug = new SmartPlug(Long.valueOf(strings[0]), Long.valueOf(strings[1]), Float.valueOf(strings[2]), Boolean.valueOf(strings[3]),
                                Long.valueOf(strings[4]), Long.valueOf(strings[5]), Long.valueOf(strings[6]));

                        return Arrays.asList(smartPlug).iterator();
                    }
                });


        smartPlugJavaRDDCumulative.cache();
        smartPlugJavaRDDInstant.cache();
    }


    /**
     * Questo metodo esegue la prima query. In input viene passato il path su cui spark effettuerà il saveAsTextFile.
     * Viene sfruttato lo smartPlugJavaRDDInstant che è un JavaRDD globale, cachato in precedenza da un altro metodo
     *
     * @param path path del file su cui scrivere l'otuput
     */
    public static void firstQuery(String path) throws FileAlreadyExistsException {
        //per risolvere la prima query è necessario lavorare con le tuple aventi property=1
        //Inizialmente prendo ogni entità e la mappo in una nuova entità chiave valore, la cui chiave è l'id della casa e il timestamp, mentre il valore è il value della presa, ovvero il consumo instantaneo.
        //È' stato scelto di implementare la query secondo cui tutte le prese della singola casa devono superare il valore dei 350 Watt.
        smartPlugJavaRDDInstant
                .mapToPair(new PairFunction<SmartPlug, Tuple2<Long, Long>, Float>() {
                    @Override
                    public Tuple2<Tuple2<Long, Long>, Float> call(SmartPlug smartPlug) throws Exception {
                        return new Tuple2<>(new Tuple2<>(smartPlug.getHouse_id(), smartPlug.getTimestamp()), smartPlug.getValue());
                        //return //Tuple2<Tuple2<house_id,timestamp>,value>
                    }
                })
                //sommo tutti valori della presa appartenenti alla stessa casa e aventi lo stesso timestamp
                .reduceByKey(new Function2<Float, Float, Float>() {
                    @Override
                    public Float call(Float aFloat, Float aFloat2) throws Exception {
                        return aFloat + aFloat2;
                    }
                })
                //filtro solo le tuple con somma dei value>350
                .filter(new Function<Tuple2<Tuple2<Long, Long>, Float>, Boolean>() {
                    @Override
                    public Boolean call(Tuple2<Tuple2<Long, Long>, Float> tuple2FloatTuple2) throws Exception {
                        return tuple2FloatTuple2._2 > GLOBAL.valueMax;
                    }
                })
                //mappo la tuple in ingresso (di formato Tuple2<Tuple2<House_id,Timestamp>,sumValue> in una nuova tupla chiave valore Tuple2<House_id,sumValue>
                .mapToPair(new PairFunction<Tuple2<Tuple2<Long, Long>, Float>, Long, Float>() {
                    @Override
                    public Tuple2<Long, Float> call(Tuple2<Tuple2<Long, Long>, Float> tuple2FloatTuple2) throws Exception {
                        return new Tuple2<>(tuple2FloatTuple2._1._1, tuple2FloatTuple2._2);
                        //return Tuple2<house_id,Value>
                    }
                })
                //raggruppo tutte le tuple aventi la stessa chiave House_id.
                .groupByKey()
                //mappo la tupla in ingresso in una nuova tupla contenente solo il valore dell'house_id
                .map(new Function<Tuple2<Long, Iterable<Float>>, Long>() {
                    @Override
                    public Long call(Tuple2<Long, Iterable<Float>> longIterableTuple2) throws Exception {
                        return longIterableTuple2._1;
                    }
                })
                //usato per indirizzare tutto all'interno di un singolo operatore per poter salvare in un unico file
                .coalesce(1)
                .saveAsTextFile(path);
    }

    /**
     * Questo metodo esegue la seconda query. In input viene passato il path su cui spark effettuerà il saveAsTextFile.
     * Viene sfruttato lo smartPlugJavaRDDCumulative che è un JavaRDD globale, cachato in precedenza da un altro metodo,
     * il quale contiene tutte le tuple aventi property=0
     *
     * @param path path del file su cui scrivere l'otuput
     */

    public static void secondQuery(String path) throws FileAlreadyExistsException {

        //per risolvere la seconda query è necessario lavorare con le tuple aventi property=0
        //Inizialmente prendo ogni entità e la mappo in una nuova tupla chiave valore, con chiave Tuple4<House_id,Plug_id,giorno,fascia_oraria> e come valore lo smartPlug
        smartPlugJavaRDDCumulative.mapToPair(new PairFunction<SmartPlug, Tuple4<Long, Long, Integer, Integer>, SmartPlug>() {
            @Override
            public Tuple2<Tuple4<Long, Long, Integer, Integer>, SmartPlug> call(SmartPlug smartPlug) throws Exception {
                //utilizzo il calendario gregoriano per ricavarmi dal timestamp il giorno e l'ora giornaliera (in 24h)
                Date date = new Date((long) smartPlug.getTimestamp() * 1000);
                Calendar calendar = GregorianCalendar.getInstance();
                calendar.setTime(date);
                int hourTimestamp = calendar.get(Calendar.HOUR_OF_DAY);
                int range = 0;
                if (hourTimestamp >= GLOBAL.firstZoneLeft && hourTimestamp < GLOBAL.firstZoneRight)
                    range = 1;
                else if (hourTimestamp >= GLOBAL.firstZoneRight && hourTimestamp < GLOBAL.secondZone)
                    range = 2;
                else if (hourTimestamp >= GLOBAL.secondZone && hourTimestamp < GLOBAL.thirdZone)
                    range = 3;
                else if (hourTimestamp >= GLOBAL.thirdZone && hourTimestamp < GLOBAL.fourthZone)
                    range = 4;

                int day = calendar.get(Calendar.DAY_OF_MONTH);

                return new Tuple2<>(new Tuple4<>(smartPlug.getHouse_id(), smartPlug.getPlug_id(), day, range), smartPlug);
            }
        })
                //raggruppo tutte le tuple con la stessa house_id, plug_id,giorno e fascia_oraria
                .groupByKey()

                //all'interno di questo mapToPair scansiono tutto l'iterable di valori con la stessa chiave.
                //Viene effettuata la scansione per cercare i value con massimo e minimo timestamp, successivametne
                //verrà effettuata la sottrazione di questi valori ed utilizzata come value nella tupla chiave valore di uscita.
                //la tupla in output sarà quindi una tupla chiave valor,e con chiave house_id, giorno e fascia_oraria, mentre come valore
                //restituisco la differenza dei valori dal timestamp più recente a quello meno recente
                .mapToPair(new PairFunction<Tuple2<Tuple4<Long, Long, Integer, Integer>, Iterable<SmartPlug>>, Tuple3<Long, Integer, Integer>, Float>() {
                    @Override
                    public Tuple2<Tuple3<Long, Integer, Integer>, Float> call(Tuple2<Tuple4<Long, Long, Integer, Integer>, Iterable<SmartPlug>> tuple3IterableTuple2) throws Exception {
                        Float firstValue = Float.valueOf(0);
                        Float lastValue = Float.valueOf(0);

                        TreeSet<SmartPlug> treeSet = new TreeSet<>();

                        tuple3IterableTuple2._2.iterator().forEachRemaining(smartPlug -> treeSet.add(smartPlug));

                        firstValue = treeSet.first().getValue();
                        lastValue = treeSet.last().getValue();
                        return new Tuple2<>(new Tuple3<>(tuple3IterableTuple2._1._1(), tuple3IterableTuple2._1._3(), tuple3IterableTuple2._1._4()), lastValue - firstValue);

                    }
                })
                //filtro tutti le tuple con differenza minore di 0 (perchè vorrebbe dire che la SmartPlug è stata resettata)
                .filter(new Function<Tuple2<Tuple3<Long, Integer, Integer>, Float>, Boolean>() {
                    @Override
                    public Boolean call(Tuple2<Tuple3<Long, Integer, Integer>, Float> tuple2FloatTuple2) throws Exception {
                        return tuple2FloatTuple2._2 > 0;
                    }
                })
                //sommo tutte le tuple con la stessa chiave. In questo modo sto sommando i valori cumulativi delle prese appartenente alla stessa casa
                .reduceByKey(new Function2<Float, Float, Float>() {
                    @Override
                    public Float call(Float aFloat, Float aFloat2) throws Exception {
                        return aFloat + aFloat2;
                    }
                })
                //mappo la tupla entrante in una nuova tupla contenente Tuple2<Tuple2<House_id,fascia_oraria>,sumValue>
                .mapToPair(new PairFunction<Tuple2<Tuple3<Long, Integer, Integer>, Float>, Tuple2<Long, Integer>, Float>() {
                    @Override
                    public Tuple2<Tuple2<Long, Integer>, Float> call(Tuple2<Tuple3<Long, Integer, Integer>, Float> tuple3FloatTuple2) throws Exception {
                        return new Tuple2<>(new Tuple2<>(tuple3FloatTuple2._1._1(), tuple3FloatTuple2._1._3()), tuple3FloatTuple2._2);
                    }
                })
                //raggruppo tutte le tuple con la stessa chiave, per poter fare la media mensile
                .groupByKey()
                //qui farò la media mensile. Scansiono l'iterable per calcolare la media e la deviazione standard
                .mapToPair(new PairFunction<Tuple2<Tuple2<Long, Integer>, Iterable<Float>>, Tuple2<Long, Integer>, Tuple2<Float, Float>>() {
                    @Override
                    public Tuple2<Tuple2<Long, Integer>, Tuple2<Float, Float>> call(Tuple2<Tuple2<Long, Integer>, Iterable<Float>> tuple2IterableTuple2) throws Exception {

                        //qui si calcola la media e la varianza

                        AtomicReference<Float> media = new AtomicReference<>(new Float(0));
                        AtomicReference<Float> devStandard = new AtomicReference<>(new Float(0));
                        AtomicInteger size = new AtomicInteger();

                        tuple2IterableTuple2._2.forEach(aFloat -> {
                            size.getAndIncrement();
                            media.set(media.get() + aFloat);
                        });

                        tuple2IterableTuple2._2.forEach(aFloat -> {
                            devStandard.set(devStandard.get() + (aFloat - media.get()) * (aFloat - media.get()));
                        });

                        media.set(media.get() / size.get());
                        devStandard.set((float) Math.sqrt((float) devStandard.get() / size.get()));

                        return new Tuple2<>(new Tuple2<>(tuple2IterableTuple2._1._1, tuple2IterableTuple2._1._2), new Tuple2<>(media.get(), devStandard.get()));

                    }
                })
                //mappo per dare un ordine all' output
                .map(new Function<Tuple2<Tuple2<Long, Integer>, Tuple2<Float, Float>>, String>() {
                    @Override
                    public String call(Tuple2<Tuple2<Long, Integer>, Tuple2<Float, Float>> tuple2Tuple2Tuple2) throws Exception {
                        return "house_id:" + tuple2Tuple2Tuple2._1._1 + " range:" + tuple2Tuple2Tuple2._1._2 + " media:" + tuple2Tuple2Tuple2._2._1 + " dev_standard:" + tuple2Tuple2Tuple2._2._2;
                    }
                })
                //confluisco il tutto all'interno di un singolo nodo, per stampare in un singolo file
                .coalesce(1)
                .saveAsTextFile(path);
    }

    /**
     * Questo metodo esegue la terza query. In input viene passato il path su cui spark effettuerà il saveAsTextFile.
     * Viene sfruttato lo smartPlugJavaRDDCumulative che è un JavaRDD globale, cachato in precedenza da un altro metodo,
     * il quale contiene tutte le tuple aventi property=0
     *
     * @param path path del file su cui scrivere l'otuput
     */

    public static void thirdQuery(String path) throws FileAlreadyExistsException {

        //per risolvere la terza query è necessario lavorare con le tuple aventi property=0
        //inizialmente mappo le entità entranti (che sono tuple<SmartPlug>) in una tupla chiave valore, con chiave Tuple3<house_id/plug_id,giorno,fascia_oraria> e con valore l'oggetto timePlug.
        //È stato scelto di creare una chiave combinata house_id/plug_id in maniera tale da distinguere tutte le prese in base alla casa (questo perchè tutte le prese hanno lo stesso identificativo 0,1 o 2).
        smartPlugJavaRDDCumulative.mapToPair(new PairFunction<SmartPlug, Tuple3<String, Integer, Integer>, SmartPlug>() {
            @Override
            public Tuple2<Tuple3<String, Integer, Integer>, SmartPlug> call(SmartPlug smartPlug) throws Exception {
                Date date = new Date((long) smartPlug.getTimestamp() * 1000);
                Calendar calendar = GregorianCalendar.getInstance();
                calendar.setTime(date);
                int hourTimestamp = calendar.get(Calendar.HOUR_OF_DAY);
                int range;
                int day = calendar.get(Calendar.DAY_OF_WEEK);
                int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                if (hourTimestamp >= GLOBAL.firstHour || hourTimestamp < GLOBAL.secondHour || day == GLOBAL.firstDay || day == GLOBAL.secondDay)
                    range = 2;    //fascia fuori punta
                else
                    range = 1; //fascia di punta
                return new Tuple2<>(new Tuple3<>(smartPlug.getHouse_id() + "/" + smartPlug.getPlug_id(), dayOfMonth, range), smartPlug);
            }
        })
                //raggruppo le tuple con la stessa chiave
                .groupByKey()
                //mappo  tutte le entità in ingresso in nuove tuple con chiave Tuple<house_id/plug_id,fascia> e come valore l'oggetto smartplug.
                //in questa fase viene scansionato l'iterable di SmartPlug generato dalla groupbykey, poi viene cercato il valore cumulativo
                //con timestamp più recente e meno recente, e viene inserita questa differenza all'interno del valore della tupla restituita in output
                .mapToPair(new PairFunction<Tuple2<Tuple3<String, Integer, Integer>, Iterable<SmartPlug>>, Tuple2<String, Integer>, Float>() {
                    @Override
                    public Tuple2<Tuple2<String, Integer>, Float> call(Tuple2<Tuple3<String, Integer, Integer>, Iterable<SmartPlug>> tuple3IterableTuple2) throws Exception {
                        Float firstValue = Float.valueOf(0);
                        Float lastValue = Float.valueOf(0);
                        ArrayList<SmartPlug> smartPlugArrayList = new ArrayList<>();

                        //creo un treeset in cui ogni volta inserisco in maniera ordinata in funzione del timestamp smartplug
                        TreeSet<SmartPlug> treeSet = new TreeSet<>();


                        tuple3IterableTuple2._2.iterator().forEachRemaining(smartPlug -> treeSet.add(smartPlug));


                        firstValue = treeSet.first().getValue();
                        lastValue = treeSet.last().getValue();

                        return new Tuple2<>(new Tuple2<>(tuple3IterableTuple2._1._1(), tuple3IterableTuple2._1._3()), lastValue - firstValue);

                    }
                })
                //filtro le tuple con solo valore >0, in maniera tale che le tuple con differenza <0 sono quelle che sono state resettate, e quindi non tenute in considerazione
                .filter(new Function<Tuple2<Tuple2<String, Integer>, Float>, Boolean>() {
                    @Override
                    public Boolean call(Tuple2<Tuple2<String, Integer>, Float> tuple2FloatTuple2) throws Exception {
                        return tuple2FloatTuple2._2 > 0;
                    }
                })
                //raggruppo tutte le tuple aventi la stessa chiave
                .groupByKey()
                //dopo la groupByKey ho una iterable di valori di float della stessa chiave Tuple2<house_id/plug_id,fascia>
                //mappo le entità entranti in una nuova tupla chiave valore con chiave House_id/Plug_Id e con valore la media dei valori dell'iterable
                .mapToPair(new PairFunction<Tuple2<Tuple2<String, Integer>, Iterable<Float>>, String, Float>() {
                    @Override
                    public Tuple2<String, Float> call(Tuple2<Tuple2<String, Integer>, Iterable<Float>> tuple2IterableTuple2) throws Exception {
                        //qui si calcola la media

                        AtomicReference<Float> media = new AtomicReference<>(new Float(0));
                        AtomicInteger size = new AtomicInteger();

                        tuple2IterableTuple2._2.forEach(aFloat -> {
                            size.getAndIncrement();
                            media.set(media.get() + aFloat);
                        });

                        media.set(media.get() / size.get());

                        //IMPORTANTE: nel caso in cui la fascia dovesse essere non di punta, setto il valore negativo in maniera tale che
                        //nella reducebyKey della stessa presa basta semplicemente fare la somma dei valori

                        if (tuple2IterableTuple2._1._2 == 2)
                            media.set(-media.get());

                        return new Tuple2<>(tuple2IterableTuple2._1._1, media.get());
                    }
                })
                //sommo i valori della stessa presa e stessa casa. Qui sommo i valori positivi e negativi, perchè mi serve restituire coem ouptu la differenza delle fasce
                .reduceByKey(new Function2<Float, Float, Float>() {
                    @Override
                    public Float call(Float aFloat, Float aFloat2) throws Exception {
                        return aFloat + aFloat2;
                    }
                })
                //inverto la chiave con il valore, poichè il sortByKey lavora con la chiave
                .mapToPair(new PairFunction<Tuple2<String, Float>, Float, String>() {
                    @Override
                    public Tuple2<Float, String> call(Tuple2<String, Float> longFloatTuple2) throws Exception {
                        return new Tuple2<>(longFloatTuple2._2, longFloatTuple2._1);
                    }
                })
                //indirizzo tutto il flusso di lavoro in un unico nodo, per poter stampare su un unico file
                .coalesce(1)
                //ordino le tuple in base ai valori della chiave, come richeide la traccia
                .sortByKey(false)
                //mappo la tupla per renderla leggibile in output
                .map(new Function<Tuple2<Float, String>, String>() {
                    @Override
                    public String call(Tuple2<Float, String> floatStringTuple2) throws Exception {
                        return "key:" + floatStringTuple2._2 + " value:" + floatStringTuple2._1;
                    }
                })                .coalesce(1)
                .saveAsTextFile(path);
    }
}
