import scala.Serializable;

public class SmartPlug implements Serializable,Comparable<SmartPlug> {

    private Long id;
    private Long timestamp;
    private Float value;
    private Boolean property;
    private Long plug_id;
    private Long household_id;
    private Long house_id;


    public SmartPlug(Long id, Long timestamp, Float value, Boolean property, Long plug_id, Long household_id, Long house_id) {
        this.id = id;
        this.timestamp = timestamp;
        this.value = value;
        this.property = property;
        this.plug_id = plug_id;
        this.household_id = household_id;
        this.house_id = house_id;
    }

    public SmartPlug() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public Boolean getProperty() {
        return property;
    }

    public void setProperty(Boolean property) {
        this.property = property;
    }

    public Long getPlug_id() {
        return plug_id;
    }

    public void setPlug_id(Long plug_id) {
        this.plug_id = plug_id;
    }

    public Long getHousehold_id() {
        return household_id;
    }

    public void setHousehold_id(Long household_id) {
        this.household_id = household_id;
    }

    public Long getHouse_id() {
        return house_id;
    }

    public void setHouse_id(Long house_id) {
        this.house_id = house_id;
    }

    @Override
    public int compareTo(SmartPlug o) {
            return compare(this.getTimestamp(), o.getTimestamp());
    }

    public static int compare(long x, long y) {
        return (x < y) ? -1 : ((x == y) ? 0 : 1);
    }
}
