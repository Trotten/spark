import jdk.nashorn.internal.objects.Global;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.rdd.RDD;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import javax.xml.crypto.Data;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.apache.spark.sql.functions.*;

public class SparkSQL {

    public static void main(String[] args){

        //Logger.getLogger("org").setLevel(Level.ERROR);
        SparkSession session=SparkSession.builder().appName("SparkSQL").master("local[*]").getOrCreate();

        Dataset<Row> dataset=session.read().csv("/Users/Valerio/Downloads/d14_filtered.csv")
                .withColumnRenamed("_c0","id")
                .withColumnRenamed("_c1","timestamp")
                .withColumnRenamed("_c2","value")
                .withColumnRenamed("_c3","property")
                .withColumnRenamed("_c4","plug_id")
                .withColumnRenamed("_c5","household_id")
                .withColumnRenamed("_c6","house_id");
        dataset=dataset.withColumn("id",col("id").cast("long"))
                .withColumn("timestamp",new Column("timestamp").cast("long"))
                .withColumn("value",new Column("value").cast("float"))
                .withColumn("property",new Column("property").cast("long"))
                .withColumn("plug_id",new Column("plug_id").cast("long"))
                .withColumn("household_id",new Column("household_id").cast("long"))
                .withColumn("house_id",new Column("house_id").cast("long"));
        //dataset=dataset.withColumn("prova",col(""));

        query1(dataset);
        query2(dataset);

        }


    public static void query1(Dataset<Row> dataset){
        Dataset<Row> myDataset=dataset.filter(col("property").equalTo(1))
                .select("house_id","value","timestamp");
        myDataset.show();
        myDataset.groupBy("house_id","timestamp").sum("value").filter(col("sum(value)").$greater(350)).groupBy("house_id").count().show();
    }

    public static void query2(Dataset<Row> dataset){



        /*dataset=dataset.filter(col("property").equalTo(0));
        String query=new String();
        Long oneDay=new Long(24*60*60);
        Long start=GLOBAL.timestampStart;
        Long end=new Long(6*60*60)+start;
        Long range=new Long(6*60*60);
        query="( timestamp > "+ start+" AND timestamp < "+end+" ) ";
        while (true){
            if (start>GLOBAL.timestampEnd)
                break;
            start=start+oneDay;
            end=range+start;

            //provo qui

            query=query+" OR ( timestamp > "+ start+" AND timestamp < "+end+" )";
        }
        System.out.println(query);

        dataset.where("( "+query+" )").groupBy("house_id").avg("value").show();*/



        dataset=dataset.filter(col("property").equalTo(0));
        String query=new String();
        Long oneDay=new Long(24*60*60);
        Long start=GLOBAL.timestampStart;
        Long end=new Long(6*60*60)+start;
        Long range=new Long(6*60*60);
        query="( timestamp > "+ start+" AND timestamp < "+end+" ) ";
        while (true){
            if (start>GLOBAL.timestampEnd)
                break;
            start=start+oneDay;
            end=range+start;


            query=" ( timestamp > "+ start+" AND timestamp < "+end+" )";
        }
        System.out.println(query);

        dataset.where("( "+query+" )").groupBy("house_id").avg("value").show();


    }
}
